# .bash_profile

### GIT ###
export HOMEBREW_GITHUB_API_TOKEN="2d4a58be82f9b000f38900daad8f52f622b53e94"


### CLI ###
# Load ~/.extra, ~/.bash_prompt, ~/.exports, ~/.aliases and ~/.functions
# ~/.extra can be used for settings you don’t want to commit
for file in ~/.{inputrc,exports,aliases,nvmrc,liquidpromptrc,git-completion.bash}; do
	[ -r "$file" ] && source "$file"
done
unset file

# Liquid Prompt
if [ -f /usr/local/share/liquidprompt ]; then
    . /usr/local/share/liquidprompt
fi

# fasd
eval "$(fasd --init auto)"

# Case-insensitive globbing (used in pathname expansion)
shopt -s nocaseglob

# Add tab completion for SSH hostnames based on ~/.ssh/config, ignoring wildcards
[ -e "$HOME/.ssh/config" ] && complete -o "default" -o "nospace" -W "$(grep "^Host" ~/.ssh/config | grep -v "[?*]" | cut -d " " -f2)" scp sftp ssh

# Add tab completion for `defaults read|write NSGlobalDomain`
# You could just use `-g` instead, but I like being explicit
# This works like an alias. you can add sharif to the ls command like this: complete -W "shairf" ls
complete -W "NSGlobalDomain" defaults
